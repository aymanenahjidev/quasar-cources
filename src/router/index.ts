import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from 'vue-router';

import routes from './routes';
import { useUserStore } from 'src/stores/user';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  Router.beforeEach(async (to) => {
    // ✅ This will work because the router starts its navigation after
    // the router is installed and pinia will be installed too
    const userStore = useUserStore();
    // Returns to login page, then authenticate by /auth/user/ then redirects to the default authentication page.
    let isCurrentUserLoggedIn = userStore.isLoggedIn;

    // if (!isCurrentUserLoggedIn) {

    // }
    isCurrentUserLoggedIn = await userStore
      .getCurrentUserAction()
      .then(() => {
        return true;
      })
      .catch(() => {
        return false;
      });

    // Is Auth page = we redirect to home if logged in
    if (isCurrentUserLoggedIn && to.meta.isAuthPage && to.name == 'login') {
      return { name: 'home' };
    }

    if (!isCurrentUserLoggedIn && to.meta.isAuthPage) return { name: 'login' };
  });

  return Router;
});
