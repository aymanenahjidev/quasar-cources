declare namespace API {
  type Cours = {
    id: number;
    title: string;
    description: string;
    catrgory: API.Category;
  };
}
