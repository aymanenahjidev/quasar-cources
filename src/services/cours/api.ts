import axiosInterceptor from 'src/utils/axiosInterceptor';

export const getCourses = () => {
  return axiosInterceptor.get<API.Cours[]>('/courses');
};
