import axiosInterceptor from 'src/utils/axiosInterceptor';

export const userLogIn = (loginData: API.UserLoginForm) => {
  const apiUrl = '/accounts/login/';

  return axiosInterceptor.post<API.UserLoggedInData>(apiUrl, loginData);
};
