declare namespace API {
  type UserLoginForm = {
    username: string;
    password: string;
  };

  type UserLoginFormValidation = {
    username: string;
    password: string;
    non_field_errors: string;
  };
  type UserLoggedInData = {
    access_token: string;
    refresh_token: string;
    user: API.User;
  };
  type MetaData = {
    isAuthPage: boolean;
  };
}
