import axiosInterceptor from 'src/utils/axiosInterceptor';

export const getUser = () => {
  const apiUrl = '/users/me/';

  return axiosInterceptor.get<API.User>(apiUrl);
};
