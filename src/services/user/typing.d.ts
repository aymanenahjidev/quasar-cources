declare namespace API {
  type User = {
    username: string;
    first_name: string;
    last_name: string;
  };
}
