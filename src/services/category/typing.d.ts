declare namespace API {
  type Category = {
    id: number;
    title: string;
    description: string;
  };
}
