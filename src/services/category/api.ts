import axiosInterceptor from 'src/utils/axiosInterceptor';

export const getCategories = () => {
  return axiosInterceptor.get<API.Category[]>('/categories');
};
