import { defineStore } from 'pinia';
import { userLogIn } from 'src/services/auth/api';
import { getUser } from 'src/services/user/api';
import deviceStorage from 'src/utils/deviceStorage';

export const useUserStore = defineStore('user', {
  state: () => ({
    isLoggedIn: false,
    currentUser: {
      first_name: '',
      last_name: '',
      username: '',
    } as API.User,
  }),

  actions: {
    async userLoginAction(data: API.UserLoginForm) {
      const response = await userLogIn(data);

      deviceStorage.saveKey('access_token', response.data.access_token);
      deviceStorage.saveKey('refresh_token', response.data.refresh_token);

      if (response.status === 200) {
        this.isLoggedIn = true;
        this.currentUser = response.data.user;
      }
      console.log(response);

      return response.data;
    },

    async userLogoutAction() {
      this.isLoggedIn = false;
      deviceStorage.destroyKey('access_token');
      deviceStorage.destroyKey('refresh_token');
    },

    async getCurrentUserAction() {
      const response = await getUser();
      if (response.status === 200) {
        this.isLoggedIn = true;
        this.currentUser = response.data;
      }

      return response.data;
    },
  },
});
